package inheritance.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.04.2017
 */
public class MemoryStorage extends Storage {
    private Note[] notes = new Note[100];
    private int i;

    public void save(Note note) {
        super.save(note);

        notes[i] = note;
        i++;
    }
}
