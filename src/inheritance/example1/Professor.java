package inheritance.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.04.2017
 */
public class Professor extends Human {
    public void teach() {
        speak();
    }
}
