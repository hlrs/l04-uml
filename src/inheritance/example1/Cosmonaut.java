package inheritance.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.04.2017
 */
public class Cosmonaut extends Human {
    public void goOutIntoSpace() {
        weight -= 10;
    }
}
