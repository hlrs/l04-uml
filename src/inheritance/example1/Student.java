package inheritance.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.04.2017
 */
public class Student extends Human {
    public void study() {
        //hard learning
        weight -= 5;
        //have a question
        speak();
    }
}
