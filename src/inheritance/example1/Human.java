package inheritance.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.04.2017
 */
public class Human {
    protected float weight;

    public void think() {
    }

    public void speak() {
    }
}
