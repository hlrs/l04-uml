package implementation.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.04.2017
 */
public class Drawing {
    private int actualX;
    private int actualY;
    private Drawable drawable;

    public void setCoordinates(int x, int y) {
        actualX = x;
        actualY = y;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public void draw() {
        drawable.draw(actualX, actualY);
    }
}
