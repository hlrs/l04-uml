package implementation.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.04.2017
 */
public class Main {
    public static void main(String[] args) {
        Drawing drawing = new Drawing();

        Point point = new Point();
        drawing.setDrawable(point);
        drawing.setCoordinates(2, 2);
        drawing.draw();
        drawing.setCoordinates(2, 4);
        drawing.draw();

        Line line = new Line(3, 0);
        drawing.setDrawable(line);
        drawing.setCoordinates(2, 4);
        drawing.draw();

        Rectangle rectangle = new Rectangle(7, 7);
        drawing.setDrawable(rectangle);
        drawing.setCoordinates(0, 0);
        drawing.draw();
    }
}
