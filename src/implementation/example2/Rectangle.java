package implementation.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.04.2017
 */
public class Rectangle implements Drawable {
    private int width;
    private int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void draw(int x, int y) {
        int rightX = x + width - 1;
        int bottomY = y + height - 1;
        //line from [x;y] to [rightX;y]
        //line from [rightX;y] to [rightX;bottomY]
        //line from [rightX;bottomY] to [x;bottomY]
        //line from [x;bottomY] to [x;y]
    }
}
