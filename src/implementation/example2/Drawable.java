package implementation.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.04.2017
 */
public interface Drawable {
    void draw(int x, int y);
}
