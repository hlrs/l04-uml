package implementation.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 28.04.2017
 */
public class Line implements Drawable {
    private int dx;
    private int dy;

    public Line(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public void draw(int x, int y) {
        int endX = x + dx;
        int endY = y + dy;
        //line from [x;y] to [endX;endY]
    }
}
