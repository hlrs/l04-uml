package implementation.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Door implements Openable {
    private boolean opened;

    public void open() {
        opened = true;
    }

    public void close() {
        opened = false;
    }

    public boolean isOpened() {
        return opened;
    }
}
