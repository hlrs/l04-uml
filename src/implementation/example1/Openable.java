package implementation.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public interface Openable {
    void open();

    void close();

    boolean isOpened();
}
