package implementation.example1;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class America implements Openable {
    private Date openDate;

    public America() {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = new GregorianCalendar(timeZone);
        calendar.set(1492, Calendar.OCTOBER, 12);
        openDate = calendar.getTime();//12.10.1492
    }

    public void open() {
    }

    public void close() {
    }

    public boolean isOpened() {
        Date currentDate = new Date();
        return currentDate.after(openDate);
    }

    public static void main(String[] args) {
        America america = new America();
        System.out.println("openDate = " + america.openDate);
    }
}
