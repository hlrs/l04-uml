package aggregation.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Classroom {
    private Table[] tables;
    private Chair[] chairs;

    private Classroom(Table[] tables, Chair[] chairs) {
        this.tables = tables;
        this.chairs = chairs;
    }
}
