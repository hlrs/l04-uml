package exercise.exercise2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.05.2017
 */
public class University {
    public int teacherCount;
    private int studentCount;
    protected float budget;

    public void matriculate(Student student) {
    }

    void setBudget(float budget) {
    }

    protected void hireTeacher() {
    }

    private void spendBudget(float amount) {
    }
}
