package exercise.exercise1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 22.05.2017
 */
public class Bike {
    public static final double Pi = 3.15;
    protected int wheelRadius;
    private float gear;
    float seatHeight;

    public void increaseGear() {
    }

    public void decreaseGear() {
    }

    void maintenance(Oil oil) {
    }

    private void install(Suspension s) {
    }
}
