package modifier._protected;

/**
 * @author Vyacheslav Gorbatykh
 * @since 19.04.2017
 */
public class HomoSapiens extends Animal {
    public void changeYourself() {
        //weight - available
        //genes - not available
    }
}