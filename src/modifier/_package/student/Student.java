package modifier._package.student;

import modifier._package.teacher.HLRS;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.04.2017
 */
public class Student {
    public void makeAPause(HLRS hlrs) {
        //hlrs.kitchen - not available
    }
}
