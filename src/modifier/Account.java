package modifier;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.04.2017
 */
public class Account {
    String ownerName;
    private int number;
    private int balance;

    public void printState() {
    }

    void calculateInterest() {
    }
}
