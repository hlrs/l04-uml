package modifier.exercise.airport;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.04.2017
 */
public class Steward {
    public void serveDinner(Plain plain) {
        //plain.seats - available
        //plain.kitchen - available
        //plain.steeringWheel - not available
    }
}
