package modifier.exercise.airport;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.04.2017
 */
public class Plain {
    public Seat[] seats;
    Kitchen kitchen;
    private SteeringWheel steeringWheel;

    void setPilot(Pilot pilot) {
        pilot.setSteeringWheel(steeringWheel);
    }
}
