package modifier.exercise;

import modifier.exercise.airport.Plain;

/**
 * @author Vyacheslav Gorbatykh
 * @since 20.04.2017
 */
public class Passenger {
    public void takeSeat(Plain plain) {
        //plain.seats - available
        //plain.kitchen - not available
        //plain.steeringWheel - not available
    }
}
