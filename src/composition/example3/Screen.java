package composition.example3;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Screen {
    public final float screenSize;

    public Screen(float screenSize) {
        this.screenSize = screenSize;
    }
}
