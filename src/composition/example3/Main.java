package composition.example3;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Main {
    public static void main(String[] args) {
        Battery battery = new Battery(3.8f);

        Phone phone = new Phone(4);
        phone.setBattery(battery);
    }
}
