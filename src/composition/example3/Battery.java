package composition.example3;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Battery {
    private float voltage;

    public Battery(float voltage) {
        this.voltage = voltage;
    }

    public float getVoltage() {
        return voltage;
    }
}
