package composition.example3;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Phone {
    private Screen screen;
    private Battery battery;

    public Phone(float screenSize) {
        screen = new Screen(screenSize);
    }

    public void setBattery(Battery battery) {
        this.battery = battery;
    }
}
