package composition.exercise;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Furniture {
    public final String name;

    public Furniture(String name) {
        this.name = name;
    }
}
