package composition.exercise;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Main {
    public static void main(String[] args) {
        Flat flat1 = new Flat(2);
        flat1.initializeRoom(1, 1);
        flat1.initializeRoom(2, 1);

        Room flat1Room1 = flat1.getRoom(1);
        flat1Room1.setFurniture(new Furniture[]{
                new Furniture("Sofa"),
                new Furniture("TV")
        });
        Room flat1Room2 = flat1.getRoom(2);
        flat1Room2.setFurniture(new Furniture[]{
                new Furniture("Bed"),
                new Furniture("Closet")
        });

        Flat flat2 = new Flat(3);
        flat2.initializeRoom(1, 2);
        flat2.initializeRoom(2, 1);
        flat2.initializeRoom(3, 1);

        move(flat1.getRoom(1), flat2.getRoom(1));
        move(flat1.getRoom(2), flat2.getRoom(3));
    }

    public static void move(Room from, Room to) {
        Furniture[] furniture = from.getFurniture();
        from.setFurniture(null);
        to.setFurniture(furniture);
    }
}
