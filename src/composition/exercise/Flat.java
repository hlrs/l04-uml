package composition.exercise;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Flat {
    private Room[] rooms;

    public Flat(int roomCount) {
        rooms = new Room[roomCount];
    }

    public void initializeRoom(int roomNumber, int windowCount) {
        rooms[roomNumber - 1] = new Room(windowCount);
    }

    public Room getRoom(int roomNumber) {
        return rooms[roomNumber - 1];
    }
}
