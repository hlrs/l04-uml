package composition.exercise;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Room {
    private Window[] windows;
    private Furniture[] furniture;

    public Room(int windowCount) {
        windows = new Window[windowCount];
        for (int i = 0; i < windows.length; i++) {
            windows[i] = new Window();
        }
    }

    public Furniture[] getFurniture() {
        return furniture;
    }

    public void setFurniture(Furniture[] furniture) {
        this.furniture = furniture;
    }
}
