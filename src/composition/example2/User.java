package composition.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class User {
    private Credentials credentials;

    public User() {
        credentials = new Credentials();
    }
}
