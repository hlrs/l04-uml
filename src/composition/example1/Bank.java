package composition.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Bank {
    private Account[] accounts;
    private int i;

    public Bank() {
        accounts = new Account[10];
    }

    public Account openAccount() {
        Account account = new Account();
        accounts[i] = account;
        i++;
        return account;
    }
}
