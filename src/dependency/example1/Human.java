package dependency.example1;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Human {
    public void call(Phone phone) {
        phone.pressButton(0);
        phone.pressButton(7);
        phone.pressButton(1);
        phone.pressButton(1);
        //...
    }
}
