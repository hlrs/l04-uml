package dependency.example5;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Sender {
    private Storage storage;

    public void send(Message message) {
        storage.save(message);
        //...
    }
}
