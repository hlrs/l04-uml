package dependency.example4;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Human {
    private Bean[] beans;
    private Water water;
    private Cup cupOfCoffee;
    private CoffeeMachine coffeeMachine;

    public void setCoffeeMachine(
            CoffeeMachine coffeeMachine) {
        this.coffeeMachine = coffeeMachine;
    }

    public void makeCoffee() {
        coffeeMachine.putBeans(beans);
        coffeeMachine.pourWater(water);
        coffeeMachine.putCup(cupOfCoffee);
        coffeeMachine.makeCoffee();
        cupOfCoffee = coffeeMachine.getCup();
    }
}
