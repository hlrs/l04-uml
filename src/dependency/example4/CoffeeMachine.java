package dependency.example4;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class CoffeeMachine {
    private Cup cup;

    public void putBeans(Bean[] beans) {
    }

    public void pourWater(Water water) {
    }

    public void putCup(Cup cup) {
        this.cup = cup;
    }

    public void makeCoffee() {
    }

    public Cup getCup() {
        return cup;
    }
}
