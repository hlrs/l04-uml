package dependency.example2;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Human {
    public void call(Phone phone) {
        phone.touchScreen(174, 219);//0
        phone.touchScreen(64, 372); //7
        phone.touchScreen(40, 709); //1
        phone.touchScreen(38, 758); //1
    }
}
