package dependency.example3;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class Human {
    private Coin coin;
    private Cup cupOfCoffee;
    private CoffeeMachine coffeeMachine;

    public void setCoffeeMachine(
            CoffeeMachine coffeeMachine) {
        this.coffeeMachine = coffeeMachine;
    }

    public void makeCoffee() {
        coffeeMachine.putCoin(coin);
        coin = null;
        coffeeMachine.chooseCoffee(42);
        cupOfCoffee = coffeeMachine.getCup();
    }
}
