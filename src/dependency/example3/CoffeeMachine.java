package dependency.example3;

/**
 * @author Vyacheslav Gorbatykh
 * @since 27.04.2017
 */
public class CoffeeMachine {
    public void putCoin(Coin coin) {
    }

    public void chooseCoffee(int variant) {
    }

    public Cup getCup() {
        return new Cup();
    }
}
